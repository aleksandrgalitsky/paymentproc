## WATI Billing microservice


**How it works**
1. The customer enters their data into TypeForm.
2. TypeForm stores this information in the Google Sheet
3. The payment service takes information every minute from Google Sheets and, if there is a new customer, creates an entry in the database.
4. Creates a new trial user in WATI.
5. WATI sends an email to the customer with a username and password for confirmation.
6. User sign in to WATI as trial account.
7. Click to "Upgrade" link.
8. WATI frontend sends request to this  microservice and gets Stripe public key. (Payment controller, getPublicKey method). Need for work Stripe JS library.
9. WATI frontend sends request to this  microservice and gets pricing plans. (Payment controller, getPlans method)
10. The customer selects a tariff plan.
11. WATI frontend sends POST request to this microservice for subscribe. (Payment controller, subscribe method). 
In parameters: domain = current domain WATI for success or close payment. 
In body JSON: 
```javascript
{
    "firstName": "John",
    "lastName": "Dow",
    "phone": "89999999999",
    "email": "jhon.dow@mail.com",
    "trialEnd": "2020-03-23T08:16:40.425Z",
    "planId": "5e733c541e8956000149c180",
    "operatorsCount": 7
  }
```
12. Microservice create Customer and Session in Stripe. Session is active 24 hour for payment. And return to WATI sessionId in response.
13. WATI call function redirectToCheckout() from Stripe JS library and following a link for payment form. 
14. User enter payment data and click Subscripe.
15. If payment is success, user will redirected to SUCCESS page in WATI.
16. In microservice Webhook controller will accepted charge result. And send this result to email and Airtable.
17. If payment success: microservice will set the default payment method for customer
18. Once a month, the payment service summarizes all messages and charges a fee
19. Webhook controller accept payment result, send emails and insert record to Airtable.

**Deploy to production**

To deploy to production, set all environment variables. The payment service will create the product and tariff plans in Stripe.

**Environment variables**
	
	StripePublicKey = from Stripe API page (e.g. "pk_test_.....") type:string
	
	StripeSecretKey = from Stripe API page (e.g. "sk_test_.....") type:string
    
	WebHookPaymentResult = from Stripe webhook page (e.g. "whsec_p.....") type:string
    
	SendGridKey = key for sending emails (e.g. "SG......") type:string
    
	PaymentNotificationEmail = billing email (e.g. "mail@mail.com,mail2@mail.com") type:string
    
	SlackPaymentNotificationEmail = billing email for Slack (e.g. "mail@mail.com") type:string
    
	AirTableAppKey = from Airtable account page (e.g. "key.....") type:string
    
	AirTableBaseId = from Airtable (e.g. "app.....") type:string

	Hangfire = Hangfire DB name (e.g. "hangfire_db_name.....") type:string

	SPREADSHEET_EMAIL = Google Service account (e.g. spreadsheet@spreadsheet.gserviceaccount.com) type:string
    
	SPREADSHEET_ID = Google Sheet ID (e.g. 1gRW.......) type:string

	SPREADSHEET_KEY_PATH = JSON file for auth (e.g. data/spreadsheet.json) type:string

	WATI-DEMO = URL WATI demo (e.g. "https://wati-demo6-server.clare.ai/") type:string
    
	WABusinessAccountId = WABA ID (e.g. 23322.......) type:string

	WABusinessAccountToken = WABA Token (e.g. 1gRW.......) type:string

	QuotaSupportMessagesRegular = Quota for support messages (Regular) (e.g. 100) type:integer

	QuotaTemplateMessagesRegular = Quota for template messages (Regular)  (e.g. 10) type:integer

	QuotaSupportMessagesPremium = Quota for support messages (Premium) (e.g. 200) type:integer

	QuotaTemplateMessagesPremium = Quota for template messages (Premium) (e.g. 20) type:integer

	TrialToken = Token for create trial users in WATI (e. g. "akjsdfh......") type:string

	SupportMessagesPrice = Price for support messages in WATI TeamInbox (e. g. 0.005) type:double

	DefaultTrialPeriod = TRIAL period in days (e. g. 7)  type:integer

	DefaultPriceEmailTrialEnd = The default price displayed in the TrialEnd email (e. g. "195.00") type:string

	WATI-DEMO-FE = URL frontend WATI TRIAL (e. g. "https://wati-demo6.clare.ai") type:string
	  

**Start in localhost**
Restore dependencies and start

- `` cd PaymentProc``

- Update the DB Connection string in `appsettings.Development.json`

```
  "ConnectionStrings": {
    "DefaultConnection": "mongodb://{username}:{password}@35.232.27.4:27017"
  }
```  
  
- ``dotnet restore``

- ``dotnet run``

## Docker deploy

**Dockerfile example**

1. Cd to server folder
``
cd PaymentProc
``

2. Build docker file
``
docker build . -t PaymentProc
``

3. Start container
``
docker run -p 5000:80 PaymentProc
``

```
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY *.sln .
COPY PaymentProc/*.csproj ./PaymentProc/

RUN dotnet restore ./PaymentProc.sln

COPY . ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out/ .

EXPOSE 80

ENTRYPOINT ["dotnet", "PaymentProc.dll"]
```