FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

COPY *.sln .
COPY PaymentProc/*.csproj ./PaymentProc/

RUN dotnet restore ./PaymentProc.sln

COPY . ./
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out/ .

EXPOSE 80

ENTRYPOINT ["dotnet", "PaymentProc.dll"]
