﻿using System;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Collections.Generic;

namespace PaymentProc.Authentication
{
    public class JwtTokenValidatorHandler : ISecurityTokenValidator
    {
        private int m_MaximumTokenByteSize;

        public bool CanValidateToken => true;

        public int MaximumTokenSizeInBytes
        {
            get
            {
                return this.m_MaximumTokenByteSize;
            }

            set
            {
                this.m_MaximumTokenByteSize = value;
            }
        }

        public JwtTokenValidatorHandler()
        {

        }

        public bool CanReadToken(string securityToken)
        {
            System.Console.WriteLine(securityToken);
            return true;
        }

        public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
        {

            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler() { };

            // Parse JWT from the Base64UrlEncoded wire form 
            //(<Base64UrlEncoded header>.<Base64UrlEncoded body>.<signature>)
            System.IdentityModel.Tokens.Jwt.JwtSecurityToken parsedJwt = tokenHandler.ReadToken(securityToken) as JwtSecurityToken;


            TokenValidationParameters validationParams =
                new TokenValidationParameters()
                {
                    ValidIssuer = BearerAuthentication.Token_Issuer,
                    ValidAudience = BearerAuthentication.Token_Issuer,
                    ValidateLifetime = true,
                    RequireExpirationTime = false,
                    SaveSigninToken = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(BearerAuthentication.SigningKey)),
                    RoleClaimType = ClaimTypes.Role,
                };

            return tokenHandler.ValidateToken(securityToken, validationParams, out validatedToken);


        }
    }

    public class BearerAuthentication
    {
        // Move to Configuration File        
        public const string Token_Issuer = "Clare_AI";
        public const string SigningKey = "4e41c3af85e9483e81f3d69ff4a2616a";

        public static string GenerateToken(string issuerSigningKey, string userName, DateTime expires, List<string> userRoles)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            claims.Add(new Claim(JwtRegisteredClaimNames.UniqueName, userName));
            claims.Add(new Claim(JwtRegisteredClaimNames.NameId, userName));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, userName));
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole.ToString()));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(issuerSigningKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(Token_Issuer,
              Token_Issuer,
              claims,
              expires: expires,
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
