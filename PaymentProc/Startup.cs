using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PaymentProc.Models;
using PaymentProc.Services;
using Hangfire;
using Hangfire.Mongo;
using PaymentProc.Attributes;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using PaymentProc.Mapping;
using Stripe;
using PaymentProc.Authentication;
using System.Text;

namespace PaymentProc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    /*.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .SetIsOriginAllowed(isOriginAllowed: _ => true);*/
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            services.AddAuthentication(opt =>
            {
                opt.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(cfg =>
            {
                //https://stackoverflow.com/questions/45686477/jwt-on-net-core-2-0

                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                cfg.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                {
                    ValidIssuer = BearerAuthentication.Token_Issuer,
                    ValidAudience = BearerAuthentication.Token_Issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(BearerAuthentication.SigningKey)),
                    ValidateLifetime = true,
                    RequireExpirationTime = false,
                    SaveSigninToken = false
                };

                //cfg.SecurityTokenValidators.Add(new JwtTokenValidatorHandler());
            });

            services.Configure<PaymentDatabaseSettings>(Configuration.GetSection(nameof(PaymentDatabaseSettings)));

            services.AddSingleton<IPaymentDatabaseSettings>(sp => sp.GetRequiredService<IOptions<PaymentDatabaseSettings>>().Value);
            services.AddSingleton<PaymentService>();
            services.AddSingleton<NotificationService>();
            services.AddSingleton<SchedulerService>();

            string connectionString = Configuration["PaymentDatabaseSettings:ConnectionString"];

            StripeConfiguration.ApiKey = Configuration["AppSettings:StripeSecretKey"];

            Mapping.MongoMapping.CreateMap();

            Migration.DefaultTrialPeriod = int.Parse(Configuration["AppSettings:DefaultTrialPeriod"]);
            Migration.CheckMigrations(Configuration["PaymentDatabaseSettings:DatabaseName"], connectionString);

            services.AddControllers();
            
            string hangfiredb = Configuration["AppSettings:Hangfire"];
            services.AddHangfire(config =>
            {
                config.UseMongoStorage(connectionString, hangfiredb, new MongoStorageOptions()
                {
                    MigrationOptions = new MongoMigrationOptions(MongoMigrationStrategy.Drop)
                });
            });
            
            services.AddHangfireServer();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            var optionsHangFire = new BackgroundJobServerOptions
            {
                ServerName = String.Format("{0}:whatsapp", System.Environment.MachineName),
                Queues = new[] { "critical", "default" }
            };

            app.UseHangfireServer(optionsHangFire);

            app.UseHangfireDashboard("/hangfire2", new DashboardOptions
            {
                Authorization = new[] { new HangfireDashboardFilter() },
                IgnoreAntiforgeryToken = true
            });

            var schedulerService = serviceProvider.GetService<SchedulerService>();
            
            RecurringJob.AddOrUpdate(() => schedulerService.CheckEndTrialAccounts(),
                 "* * * * *");

            RecurringJob.AddOrUpdate(() => schedulerService.CheckNewRowsGoogleSheets(),
                 "* * * * *");

            RecurringJob.AddOrUpdate(() => schedulerService.EnvironmentReady(),
                 "* * * * *");

            RecurringJob.AddOrUpdate(() => schedulerService.ChargeStripe(),
                 "0 0 1 * *");
        }
    }
}
