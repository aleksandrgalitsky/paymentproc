﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PaymentProc.Models;
using PaymentProc.Services;
using Stripe;
using Stripe.Checkout;

namespace PaymentProc.Controllers
{
    [Authorize]
    [Route("api/v1/webhook")]
    [ApiController]
    public class WebhookController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<WebhookController> _logger;
        private readonly PaymentService _paymentService;
        private readonly NotificationService _notificationService;

        private readonly string _trialToken;
        private readonly string _webHookPaymentResult;

        public WebhookController
            (
                ILogger<WebhookController> logger, 
                IConfiguration configuration, 
                PaymentService paymentService,
                NotificationService notificationService
            )
        {
            _logger = logger;
            _configuration = configuration;
            _paymentService = paymentService;
            _notificationService = notificationService;

            _webHookPaymentResult = _configuration["AppSettings:WebHookPaymentResult"];
            _trialToken = _configuration["AppSettings:TrialToken"];
        }

        [AllowAnonymous]
        [HttpPost("result")]
        public async Task<IActionResult> StripeWebhookPaymentResult()
        {
            using (var reader = new StreamReader(Request.Body))
            {
                string body = await reader.ReadToEndAsync();
                var client = new HttpClient();

                try
                {
                    string header = Request.Headers["Stripe-Signature"].ToString();
                    
                    Event stripeEvent = EventUtility.ConstructEvent(body, header, _webHookPaymentResult, throwOnApiVersionMismatch: false);

                    if (stripeEvent.Type == Events.CheckoutSessionCompleted)
                    {
                        var session = stripeEvent.Data.Object as Stripe.Checkout.Session;
                        
                        await _paymentService.SessionComplete(session.Id);
                    }
                    else if (stripeEvent.Type == Events.PaymentIntentSucceeded)
                    {
                        PaymentIntent pi = stripeEvent.Data.Object as PaymentIntent;
                                          
                        await _paymentService.PaymentSuccess(pi);
                    }
                    else if (stripeEvent.Type == Events.PaymentIntentPaymentFailed)
                    {
                        PaymentIntent pi = stripeEvent.Data.Object as PaymentIntent;

                        await _paymentService.PaymentFailed(pi);
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(new { e.Message });
                }

                return Ok();
            }
        }

        [AllowAnonymous]
        [HttpGet("setExpireDate")]
        public async Task<ActionResult> SetExpireDate(string email, long date, string token)
        {
            if(token == _trialToken)
            {
                if(_paymentService.SetExpireDate(email, date))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        [AllowAnonymous]
        [HttpGet("trialConfirmed")]
        public async Task<ActionResult> TrialConfirmed(string email, string token)
        {
            if (token == _trialToken)
            {
                if (_paymentService.TrialConfirmed(email))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }
    }
}