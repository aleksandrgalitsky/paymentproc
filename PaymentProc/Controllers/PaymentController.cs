﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PaymentProc.Models;
using PaymentProc.Models.Enums;
using PaymentProc.Services;
using PaymentProc.ViewModels;
using Stripe;

namespace PaymentProc.Controllers
{
    [Authorize]
    [Route("api/v1/payment")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<PaymentController> _logger;
        
        private readonly PaymentService _paymentService;

        private readonly string _stripePublicKey;

        public PaymentController
            (
                ILogger<PaymentController> logger,
                IConfiguration configuration,
                PaymentService paymentService
            )
        {
            _logger = logger;
            _configuration = configuration;
            _paymentService = paymentService;

            _stripePublicKey = _configuration["AppSettings:StripePublicKey"];
        }

        [AllowAnonymous]
        [HttpGet("getPublicKey")]
        public IActionResult GetPublicKey()
        {
            return Ok(new { PublicKey = _stripePublicKey });
        }

        [AllowAnonymous]
        [HttpGet("getPrice/{country}")]
        public IActionResult GetPrice(string country)
        {
            if(string.IsNullOrEmpty(country))
            {
                return BadRequest();
            }
            else
            {
                return Ok(new { Price = _paymentService.GetPrice(country) });
            }
        }

        [AllowAnonymous]
        [HttpGet("getPlans")]
        public async Task<IActionResult> GetPlans()
        {
            var result = _paymentService.GetPlans();
            return Ok(new { result });
        }

        [AllowAnonymous]
        [HttpPost("subsribe/{domain}")]
        public IActionResult Sibscribe(string domain, [FromBody] BillingCustomer billingCustomer)
        {
            if (billingCustomer.OperatorsCount < 1 && billingCustomer.OperatorsCount > 20)
            {
                return BadRequest();
            }

            string sessionId = _paymentService.Subscribe(domain, billingCustomer);

            if(string.IsNullOrEmpty(sessionId))
            {
                return BadRequest();
            }

            return Ok(new { SessionId = sessionId });
        }
    }
}
