﻿using MongoDB.Driver;
using MongoDBMigrations;
using PaymentProc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Mapping
{
    public class Migration_2020_04_09 : IMigration
    {
        public MongoDBMigrations.Version Version => new MongoDBMigrations.Version(1, 2, 0);
        public string Name => "Update trial accounts";
        public void Up(IMongoDatabase database)
        {
            var userCollection = database.GetCollection<TrialAccount>("TrialAccount");

            var users = userCollection.AsQueryable().ToList();

            foreach (var user in users)
            {
                if(user.TrialExpiredAt < user.SubmitedAt)
                {
                    user.TrialExpiredAt = user.SubmitedAt.AddDays(Migration.DefaultTrialPeriod);
                }
                user.Confirmed = false;

                userCollection.FindOneAndReplace(x => x.Id == user.Id, user);
            }
        }

        public void Down(IMongoDatabase database)
        {
            // ...
        }
    }
}
