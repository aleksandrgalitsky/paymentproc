﻿using MongoDB.Driver;
using MongoDBMigrations;
using PaymentProc.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace PaymentProc.Mapping
{
    public class Migration_Init : IMigration
    {
        public MongoDBMigrations.Version Version => new MongoDBMigrations.Version(1, 0, 0);
        public string Name => "Init user, prices, product and plans";
        public void Up(IMongoDatabase database)
        {
            var plans = CreateProductAndPlansInStripe();
            IMongoCollection<PricingPlan> plansCollection = database.GetCollection<PricingPlan>("PricingPlan");
            foreach (var plan in plans)
            {
                plansCollection.InsertOne(plan);
            }

            IMongoCollection<User> userCollection = database.GetCollection<User>("User");
            User user = new User()
            {
                Password = "A10|va9W%vuzSL7HqLm5",
                Username = "admin",
                Role = "ADMINISTRATOR"
            };
            userCollection.InsertOne(user);

            var pricePath = @"data/message_official_price.json";
            StreamReader objStreamReader = new StreamReader(pricePath);
            var jsonPrice = objStreamReader.ReadToEnd();
            objStreamReader.Close();

            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            List<MessagePrice> prices = JsonSerializer.Deserialize<List<MessagePrice>>(jsonPrice, options);

            IMongoCollection<MessagePrice> pricesCollection = database.GetCollection<MessagePrice>("MessagePrice");
            foreach(var price in prices)
            {
                pricesCollection.InsertOne(price);
            }
        }

        public void Down(IMongoDatabase database)
        {
            // ...
        }

        private List<PricingPlan> CreateProductAndPlansInStripe()
        {
            string _productTitle = "WhatsApp TeamInbox";
            
            var optionsProductCreate = new ProductCreateOptions
            {
                Name = _productTitle,
                Type = "service"
            };
            var serviceProduct = new ProductService();
            var product = serviceProduct.Create(optionsProductCreate);

            var servicePlan = new PlanService();

            List<PricingPlan> pricingPlans = new List<PricingPlan>();

            for (int i = 1; i <= 20; i++)
            {
                var optionsPlanCreateM = new PlanCreateOptions
                {
                    Product = product.Id,
                    Active = true,
                    AmountDecimal = 3900 * i,
                    BillingScheme = "per_unit",
                    Currency = "usd",
                    Interval = "month",
                    IntervalCount = 1,
                    Nickname = $"Recurring Regular Monthly {i.ToString()}",
                    UsageType = "licensed"
                };

                var optionsPlanCreateY = new PlanCreateOptions
                {
                    Product = product.Id,
                    Active = true,
                    AmountDecimal = 3000 * i * 12,
                    BillingScheme = "per_unit",
                    Currency = "usd",
                    Interval = "year",
                    IntervalCount = 1,
                    Nickname = $"Recurring Regular Annually {i.ToString()}",
                    UsageType = "licensed"
                };

                Plan planRRM = servicePlan.Create(optionsPlanCreateM);
                Plan planRRA = servicePlan.Create(optionsPlanCreateY);

                var ppRRM = new PricingPlan()
                {
                    Period = "monthly",
                    Title = "Regular",
                    PlanId = planRRM.Id,
                    OperatorsCount = i,
                    BillingScheme = "per_unit"
                };
                var ppRRA = new PricingPlan()
                {
                    Period = "yearly",
                    Title = "Regular",
                    PlanId = planRRA.Id,
                    OperatorsCount = i,
                    BillingScheme = "per_unit"
                };

                pricingPlans.Add(ppRRM);
                pricingPlans.Add(ppRRA);
            }

            for (int i = 5; i <= 20; i++)
            {
                var optionsPlanCreateRPM = new PlanCreateOptions
                {
                    Product = product.Id,
                    Active = true,
                    AmountDecimal = 3900 * i,
                    BillingScheme = "per_unit",
                    Currency = "usd",
                    Interval = "month",
                    IntervalCount = 1,
                    Nickname = $"Recurring Premium Monthly {i.ToString()}",
                    UsageType = "licensed"
                };

                var optionsPlanCreateRPA = new PlanCreateOptions
                {
                    Product = product.Id,
                    Active = true,
                    AmountDecimal = 3600 * i * 12,
                    BillingScheme = "per_unit",
                    Currency = "usd",
                    Interval = "year",
                    IntervalCount = 1,
                    Nickname = $"Recurring Premium Annually {i.ToString()}",
                    UsageType = "licensed"
                };

                Plan planRPM = servicePlan.Create(optionsPlanCreateRPM);
                Plan planRPA = servicePlan.Create(optionsPlanCreateRPA);


                var ppRPM = new PricingPlan()
                {
                    Period = "monthly",
                    Title = "Premium",
                    PlanId = planRPM.Id,
                    OperatorsCount = i,
                    BillingScheme = "per_unit"
                };
                var ppRPA = new PricingPlan()
                {
                    Period = "yearly",
                    Title = "Premium",
                    PlanId = planRPA.Id,
                    OperatorsCount = i,
                    BillingScheme = "per_unit"
                };

                pricingPlans.Add(ppRPM);
                pricingPlans.Add(ppRPA);
            }

            return pricingPlans;
        }
    }
}
