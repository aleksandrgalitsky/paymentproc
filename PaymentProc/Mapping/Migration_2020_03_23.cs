﻿using MongoDB.Driver;
using MongoDBMigrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Mapping
{
    public class Migration_2020_03_23 : IMigration
    {
        public MongoDBMigrations.Version Version => new MongoDBMigrations.Version(1, 1, 0);
        public string Name => "Added WABAID to Environment collection";
        public void Up(IMongoDatabase database)
        {
            IMongoCollection<Models.Environment> envsCollection = database.GetCollection<Models.Environment>("Environment");

            var builder = Builders<Models.Environment>.Filter;

            var filterWABAID = builder.Exists("WABAID", false);
            var updateWABAID = Builders<Models.Environment>.Update.Set("WABAID", "");
            var resultWABAID = envsCollection.UpdateMany(filterWABAID, updateWABAID);
        }

        public void Down(IMongoDatabase database)
        {
            // ...
        }
    }
}
