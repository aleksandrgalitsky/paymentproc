﻿using MongoDBMigrations;
using MongoDBMigrations.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Mapping
{
    public static class Migration
    {
        public static int DefaultTrialPeriod;

        public static void CheckMigrations(string bdname, string connectionString)
        {
            var options = new MigrationRunnerOptions
            {
                ConnectionString = connectionString,
                DatabaseName = bdname,
                IsSchemeValidationActive = false,
            };

            var runner = new MigrationRunner(options);

            runner.Locator.LookInAssemblyOfType<Migration_Init>();
            runner.Locator.LookInAssemblyOfType<Migration_2020_03_23>();
            runner.Locator.LookInAssemblyOfType<Migration_2020_04_09>();

            runner.UpdateTo(new MongoDBMigrations.Version(1, 2, 0));
        }
    }
}
