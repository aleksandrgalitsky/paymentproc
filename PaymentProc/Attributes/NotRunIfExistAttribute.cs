﻿using Hangfire;
using Hangfire.Common;
using Hangfire.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Attributes
{
    public class NotRunIfExistAttribute : JobFilterAttribute, IElectStateFilter
    {
        public void OnStateElection(ElectStateContext context)
        {
            if (context.CandidateState.Name == "Processing")
            {
                var monitorApi = JobStorage.Current.GetMonitoringApi();
                var jobList = monitorApi.ProcessingJobs(0, 1000).ToList();
                var isJobRunning = jobList.Count(x => x.Value.Job != null && x.Value.Job.Type.FullName == context.BackgroundJob.Job.Method.Name) > 0;

                if (isJobRunning)
                {
                    var deleteState = new DeletedState()
                    {
                        Reason = "deleting concurrent execution"
                    };
                    context.CandidateState = deleteState;
                }
            }
        }
    }
}
