﻿using Hangfire.Dashboard;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using PaymentProc.Authentication;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentProc.Attributes
{
    public class HangfireDashboardFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            var httpContext = context.GetHttpContext();
            var accessToken = httpContext.Request.Cookies["token"];
            StringValues qsToken = new StringValues();
            var qs = httpContext.Request.Query.TryGetValue("token", out qsToken);

            if (accessToken == null || (qs && accessToken != qsToken.First()))
            {
                if (qsToken.Any())
                {
                    accessToken = qsToken.First();
                    httpContext.Response.Cookies.Append("token", accessToken);
                }
            }

            if (accessToken == null)
            {
                return false;
            }

            var tokenHandler = new JwtTokenValidatorHandler();
            SecurityToken token = null;
            TokenValidationParameters validationParams = new TokenValidationParameters()
            {
                ValidIssuer = BearerAuthentication.Token_Issuer,
                ValidAudience = BearerAuthentication.Token_Issuer,
                ValidateLifetime = true,
                RequireExpirationTime = false,
                SaveSigninToken = false,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(BearerAuthentication.SigningKey)),
            };

            try
            {
                var identity = tokenHandler.ValidateToken(accessToken, validationParams, out token);

                if (token == null)
                {
                    return false;
                }

                httpContext.User = identity;
                return identity.Identity.IsAuthenticated;
            }
            catch
            {
                return false;
            }

        }
    }
}
