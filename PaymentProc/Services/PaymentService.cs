﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using PaymentProc.Models;
using PaymentProc.Models.Enums;
using PaymentProc.ViewModels;
using Stripe;
using Stripe.Checkout;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using System.Net.Http;
using Newtonsoft.Json;

namespace PaymentProc.Services
{
    public class PaymentService
    {
        private readonly IConfiguration _configuration;

        private readonly IMongoCollection<Payment> _payments;
        private readonly IMongoCollection<PricingPlan> _plans;
        private readonly IMongoCollection<BillingSession> _billingSessions;
        private readonly IMongoCollection<BillingCustomer> _billingCustomer;
        private readonly IMongoCollection<Models.Environment> _envCollection;
        private readonly IMongoCollection<TrialAccount> _trialAccounts;
        private readonly IMongoCollection<MessagePrice> _messagePriceCollection;

        private SessionService _sessionService;
        private CustomerService _customerService;
        private PaymentMethodService _paymentMethodService;

        private readonly NotificationService _notificationService;

        private readonly string _paymentNotificationEmail;
        private readonly string _slackPaymentNotificationEmail;

        private readonly string _googleSheetId;
        private readonly string _googleSheetKeyPath;

        private readonly string _trialToken;

        private readonly string _WATI_DEMO;

        private readonly string _notificationRegularQuota;
        private readonly string _supportRegularQuota;
        private readonly string _notificationPremiumQuota;
        private readonly string _supportPremiumQuota;

        static string[] Scopes = { SheetsService.Scope.Spreadsheets };

        public PaymentService(IPaymentDatabaseSettings settings, IConfiguration configuration, NotificationService notificationService)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _payments = database.GetCollection<Payment>("Payment");
            _plans = database.GetCollection<PricingPlan>("PricingPlan");
            _billingSessions = database.GetCollection<BillingSession>("BillingSession");
            _billingCustomer = database.GetCollection<BillingCustomer>("BillingCustomer");
            _envCollection = database.GetCollection<Models.Environment>("Environment");
            _trialAccounts = database.GetCollection<TrialAccount>("TrialAccount");
            _messagePriceCollection = database.GetCollection<MessagePrice>("MessagePrice");

            _notificationService = notificationService;

            _configuration = configuration;
            _paymentNotificationEmail = _configuration["AppSettings:PaymentNotificationEmail"];
            _slackPaymentNotificationEmail = _configuration["AppSettings:SlackPaymentNotificationEmail"];
            _googleSheetId = _configuration["AppSettings:SPREADSHEET_ID"];
            _googleSheetKeyPath = _configuration["AppSettings:SPREADSHEET_KEY_PATH"];
            _WATI_DEMO = _configuration["AppSettings:WATI-DEMO"];
            _trialToken = _configuration["AppSettings:TrialToken"];
            _notificationRegularQuota = _configuration["AppSettings:QuotaTemplateMessagesRegular"];
            _supportRegularQuota = _configuration["AppSettings:QuotaSupportMessagesRegular"];
            _notificationPremiumQuota = _configuration["AppSettings:QuotaTemplateMessagesPremium"];
            _supportPremiumQuota = _configuration["AppSettings:QuotaSupportMessagesPremium"];

            _sessionService = new SessionService();
            _customerService = new CustomerService();
            _paymentMethodService = new PaymentMethodService();
        }

        public List<PricingPlan> GetPlans()
        {
            var plans = _plans.AsQueryable().ToList();
            
            return plans;
        }

        public Customer CreateCustomer(BillingCustomer billingCustomer)
        {
            var customerOptions = new CustomerCreateOptions
            {
                Name = $"{billingCustomer.FirstName} {billingCustomer.LastName}",
                Email = billingCustomer.Email,
                Phone = billingCustomer.Phone,
                Description = $"Create from payment service at {DateTime.UtcNow}"
            };
            Customer customer = _customerService.Create(customerOptions);

            billingCustomer.StripeCustomerId = customer.Id;
            _billingCustomer.InsertOne(billingCustomer);

            return customer;
        }

        public Session CreateSession(string domain, Customer customer, string planId)
        {
            var plan = _plans.AsQueryable().Where(x => x.Id == planId).FirstOrDefault();
            List<SessionSubscriptionDataItemOptions> plans = new List<SessionSubscriptionDataItemOptions>();

            plans.Add(new SessionSubscriptionDataItemOptions() { Plan = plan.PlanId });

            string id = Guid.NewGuid().ToString();

            var options = new SessionCreateOptions
            {
                PaymentMethodTypes = new List<string> { "card" },
                SubscriptionData = new SessionSubscriptionDataOptions
                {
                    Items = plans
                },
                SuccessUrl = $"http://{domain}/payment/{id}",
                CancelUrl = $"http://{domain}/dashboard",
                Customer = customer.Id,

                PaymentIntentData = new SessionPaymentIntentDataOptions() { }
            };
            
            Session session = _sessionService.Create(options);
            
            BillingSession billingSession = new BillingSession()
            {
                Created = DateTime.UtcNow,
                Domain = domain,
                SessionGuid = id,
                SessionId = session.Id,
                PaymentIntentId = session.PaymentIntentId == null ? null : session.PaymentIntentId,
                CustomerId = customer.Id
            };

            _billingSessions.InsertOne(billingSession);

            return session;
        }

        public string Subscribe(string domain, BillingCustomer billingCustomer)
        {
            Customer customer = CreateCustomer(billingCustomer);

            Session session = CreateSession(domain, customer, billingCustomer.PlanId);

            return session.Id;
        }

        public void SetDefaultPaymentMethod(string customerId)
        {
            var optionsPaymentMethodList = new PaymentMethodListOptions
            {
                Customer = customerId,
                Type = "card",
            };
            PaymentMethod pm = _paymentMethodService.List(optionsPaymentMethodList).FirstOrDefault();

            var optionsCustomerUpdate = new CustomerUpdateOptions
            {
                InvoiceSettings = new CustomerInvoiceSettingsOptions() { DefaultPaymentMethod = pm.Id }
            };
            _customerService.Update(customerId, optionsCustomerUpdate);

            var optionsPaymentMethodAttach = new PaymentMethodAttachOptions
            {
                Customer = customerId,
            };
            _paymentMethodService.Attach(pm.Id, optionsPaymentMethodAttach);            
        }

        public Session GetSession(string sessionId)
        {
            var service = new SessionService();
            return service.Get(sessionId);
        }

        public async Task SessionComplete(string sessionId)
        {
            var billingSession = _billingSessions.AsQueryable().Where(x => x.SessionId == sessionId).FirstOrDefault();
            billingSession.IsComplete = true;
            _billingSessions.FindOneAndReplace(x => x.Id == billingSession.Id, billingSession);

            Customer customer = _customerService.Get(billingSession.CustomerId);
            
            var billCustomer = _billingCustomer.AsQueryable().Where(x => x.StripeCustomerId == customer.Id).FirstOrDefault();
            
            var trialAcc = _trialAccounts.AsQueryable().Where(x => x.Email == billCustomer.Email).FirstOrDefault();
            if (trialAcc != null)
            {
                trialAcc.TrialEnd = true;
                _trialAccounts.FindOneAndReplace(x => x.Id == trialAcc.Id, trialAcc);
            }
            var plan = _plans.AsQueryable().Where(x => x.Id == billCustomer.PlanId).FirstOrDefault();
            string period = plan.Period.Contains("monthly") ? "Monthly" : "Annual";
            string planTier = $"{plan.Title}{period}";
            int nQuota = plan.Title.Contains("Regular", StringComparison.InvariantCultureIgnoreCase) ? int.Parse(_notificationRegularQuota) : int.Parse(_notificationPremiumQuota);
            int sQuota = plan.Title.Contains("Regular", StringComparison.InvariantCultureIgnoreCase) ? int.Parse(_supportRegularQuota) : int.Parse(_supportPremiumQuota);

            Models.Environment env = new Models.Environment()
            {
                CustomerId = billCustomer.Id,
                IsActive = false,
                EnvironmentName = null,
                PlanTier = planTier,
                SentEmail = false,
                Url = null,
                LastBillableDate = DateTimeOffset.FromUnixTimeSeconds(1546272000).UtcDateTime,
                NotificationQuota = nQuota,
                SupportQuota = sQuota
            };

            _envCollection.InsertOne(env);

            SetDefaultPaymentMethod(customer.Id);

            await UpdateGoogleSheets(billCustomer);

            await UpdateTrialAccountInWATI(billCustomer.Email);

            // send onboarding email
            await SendOnboardingEmail(billCustomer);
        }

        private async Task SendInvoiceEmail(Customer customer, Invoice invoice, BillingCustomer billCustomer, string titleInEmail)
        {
            // send invoice to the customer
            Console.WriteLine($"SendInvoiceEmail: start cus_id={customer.Id}");
            var templateFilePath = @"EmailTemplates/PaymentTemplate.html";
            StreamReader objStreamReader = new StreamReader(templateFilePath);
            var body = objStreamReader.ReadToEnd();
            objStreamReader.Close();

            var plan = _plans.AsQueryable().Where(x => x.Id == billCustomer.PlanId).FirstOrDefault();
            StringBuilder emailTemplate = new StringBuilder(body);

            var amount = invoice.AmountPaid.ToString();
            var itemDescription = invoice.BillingReason.Contains("manual") ? invoice.Lines.First().Description :
                    $"WhatsApp TeamInbox: {plan.Title} with {plan.OperatorsCount} operators ({plan.Period})";

            amount = amount.Insert(amount.Length - 2, ".");
            emailTemplate.Replace("{fullName}", billCustomer.FirstName + " " + billCustomer.LastName);
            emailTemplate.Replace("{phone}", billCustomer.Phone);
            emailTemplate.Replace("{address}", customer.Address == null ? "" : customer.Address.ToString());
            emailTemplate.Replace("{invoiceNumber}", invoice.Number);
            emailTemplate.Replace("{invoiceDate}", DateTime.UtcNow.ToString());
            emailTemplate.Replace("{paymentDue}", "");
            emailTemplate.Replace("{amount}", amount);            
            emailTemplate.Replace("{item}", itemDescription);
            emailTemplate.Replace("{email}", billCustomer.Email);
            emailTemplate.Replace("{year}", DateTime.UtcNow.Year.ToString());
            emailTemplate.Replace("{title}", titleInEmail);

            var res = await _notificationService.SendMail(billCustomer.Email, "WATI.io - Payment", emailTemplate.ToString());
        }

        private async Task SendOnboardingEmail(BillingCustomer billCustomer)
        {
            // send invoice to the customer
            var templateFilePath = @"EmailTemplates/OnboardingTemplate.html";
            StreamReader objStreamReader = new StreamReader(templateFilePath);
            var body = objStreamReader.ReadToEnd();
            objStreamReader.Close();

            StringBuilder emailTemplate = new StringBuilder(body);

            emailTemplate.Replace("{name}", billCustomer.FirstName);
            emailTemplate.Replace("{year}", DateTime.UtcNow.Year.ToString());

            var res = await _notificationService.SendMail(billCustomer.Email, "WATI.io - Onboarding Guide", emailTemplate.ToString());
        }

        public async Task UpdateTrialAccountInWATI(string userEmail)
        {
            string baseUrl = $"{_WATI_DEMO}api/v1/accounts/accountPaid?";

            HttpClient client = new HttpClient();

            string url = $"{baseUrl}email={userEmail}&token={_trialToken}";

            Console.WriteLine($"UpdateTrialAccountInWATI: Update Account with url: {url}");

            var resp = await client.GetAsync(url);

            if (resp.StatusCode != System.Net.HttpStatusCode.OK)
            {
                string htmlMsg = $"Error updating trial account in WATI TRIAL: Email={userEmail}";
                string[] emails = _paymentNotificationEmail.Split(',');

                foreach (var email in emails)
                {
                    await _notificationService.SendMail(email, $"WATI.io - Update TRIAL account error (status code: {resp.StatusCode})", htmlMsg);
                }
            }
        }

        public async Task PaymentSuccess(PaymentIntent paymentIntent)
        {
            Customer customer = _customerService.Get(paymentIntent.CustomerId);
            var billingCustomer = _billingCustomer.AsQueryable().Where(x => x.StripeCustomerId == paymentIntent.CustomerId).FirstOrDefault();
            var watiEnv = _envCollection.AsQueryable().Where(x => x.CustomerId == billingCustomer.Id).FirstOrDefault();
            Payment payment = new Payment()
            {
                PaymentIntentId = paymentIntent.Id,
                Status = "success",
                EnviromentName = watiEnv == null ? null : watiEnv.EnvironmentName
            };

            _payments.InsertOne(payment);

            var invoiceService = new InvoiceService();
            var invoice = invoiceService.Get(paymentIntent.InvoiceId);

            await SendInvoiceEmail(customer, invoice, billingCustomer, "Payment success");

            // send to email upon stripe confirmation
            string enviromentName = watiEnv == null ? "" : watiEnv.EnvironmentName;
            string htmlMsg = $"Email: {billingCustomer.Email}<br/> {billingCustomer.FirstName} {billingCustomer.LastName}<br/> Phone:{billingCustomer.Phone}<br/> EnviromentName: {enviromentName}<br/>{JsonConvert.SerializeObject(paymentIntent)}";
            string[] emails = _paymentNotificationEmail.Split(',');

            foreach (var email in emails)
            {
                await _notificationService.SendMail(email, "WATI.io - paid successfully", htmlMsg);
            }

            string htmlSlackMsg = $"Email: {billingCustomer.Email}, {billingCustomer.FirstName} {billingCustomer.LastName}, Phone:{billingCustomer.Phone}, EnviromentName: {enviromentName}";
            await _notificationService.SendMail(_slackPaymentNotificationEmail, "WATI.io -  paid successfully", htmlSlackMsg);

            await _notificationService.SendToAirTable
                    (
                        billingCustomer.FirstName,
                        billingCustomer.LastName,
                        billingCustomer.Company,
                        billingCustomer.Email,
                        billingCustomer.Phone,
                        paymentIntent.Id, 
                        "success"
                    );
        }

        public async Task PaymentFailed(PaymentIntent paymentIntent)
        {
            Customer customer = _customerService.Get(paymentIntent.CustomerId);
            var billingCustomer = _billingCustomer.AsQueryable().Where(x => x.StripeCustomerId == paymentIntent.CustomerId).FirstOrDefault();
            var watiEnv = _envCollection.AsQueryable().Where(x => x.CustomerId == billingCustomer.Id).FirstOrDefault();
            Payment payment = new Payment()
            {
                PaymentIntentId = paymentIntent.Id,
                Status = "failed",
                EnviromentName = watiEnv == null ? null : watiEnv.EnvironmentName
            };

            _payments.InsertOne(payment);
            string enviromentName = watiEnv == null ? "" : watiEnv.EnvironmentName;
            string htmlMsg = $"Email: {billingCustomer.Email}, {billingCustomer.FirstName} {billingCustomer.LastName}, Phone:{billingCustomer.Phone}, EnviromentName: {enviromentName}";
            string[] emails = _paymentNotificationEmail.Split(',');

            foreach (var email in emails)
            {
                await _notificationService.SendMail(email, "WATI.io - paid failed", htmlMsg);
            }

            string htmlSlackMsg = $"Email: {billingCustomer.Email}, {billingCustomer.FirstName} {billingCustomer.LastName}, Phone:{billingCustomer.Phone}, EnviromentName: {enviromentName}";
            await _notificationService.SendMail(_slackPaymentNotificationEmail, "WATI.io - paid failed", htmlSlackMsg);

            await _notificationService.SendToAirTable
                    (
                        billingCustomer.FirstName,
                        billingCustomer.LastName,
                        billingCustomer.Company,
                        billingCustomer.Email,
                        billingCustomer.Phone,
                        paymentIntent.Id, 
                        "failed"
                    );
        }

        private SheetsService GetSheetsService()
        {
            using (var stream = new FileStream(_googleSheetKeyPath, FileMode.Open, FileAccess.Read))
            {
                var serviceInitializer = new BaseClientService.Initializer
                {
                    HttpClientInitializer = GoogleCredential.FromStream(stream).CreateScoped(Scopes)
                };
                return new SheetsService(serviceInitializer);
            }
        }

        public async Task UpdateGoogleSheets(BillingCustomer billingCustomer)
        {
            Console.WriteLine("UpdateGoogleSheets...");
            var env = _envCollection.AsQueryable().Where(x => x.CustomerId == billingCustomer.Id).FirstOrDefault();
            var trialAcc = _trialAccounts.AsQueryable().Where(x => x.Email.ToLower().Contains(billingCustomer.Email.ToLower())).FirstOrDefault();
            
            if (env != null)
            {
                string planTier = env.PlanTier;
                string paidDate = DateTime.UtcNow.ToString();

                var service = GetSheetsService();

                int row = trialAcc.RowInGoogleSheet;

                String spreadsheetId = _googleSheetId;
                var valueRange = new ValueRange { Values = new List<IList<object>> { new List<object> { "TRUE", paidDate, planTier } } };

                // Row NOP is Paid, PaidDate, Subscription Plan
                var update = service.Spreadsheets.Values.Update(valueRange, spreadsheetId, $"O{row}:Q{row}");
                update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
                var res = await update.ExecuteAsync();
            }
        }

        public string GetPrice(string country)
        {
            var price = _messagePriceCollection.AsQueryable().Where(x => x.Code.ToLower() == country.ToLower()).FirstOrDefault();
            if(price == null)
            {
                price = _messagePriceCollection.AsQueryable().Where(x => x.Code.ToLower() == "xx").FirstOrDefault();
            }

            return (price.Usd * 1.2).ToString();
        }

        public bool SetExpireDate(string email, long timestamp)
        {
            var date = DateTimeOffset.FromUnixTimeSeconds(timestamp).DateTime.ToLocalTime();
            Console.WriteLine($"SetExpireDate for user {email}, set expire date={date}");
            var user = _trialAccounts.AsQueryable().Where(x => x.Email == email).FirstOrDefault();

            if(user == null)
            {
                Console.WriteLine($"SetExpireDate for user is null");
                return false;
            }
            else
            {
                user.TrialExpiredAt = date;
                _trialAccounts.FindOneAndReplace(x => x.Id == user.Id, user);
                Console.WriteLine($"SetExpireDate for user SUCCESS");
                return true;
            }
        }

        public bool TrialConfirmed(string email)
        {
            Console.WriteLine($"TrialConfirmed: user {email}");
            var user = _trialAccounts.AsQueryable().Where(x => x.Email == email).FirstOrDefault();

            if (user == null)
            {
                Console.WriteLine($"TrialConfirmed: user is null");
                return false;
            }
            else
            {
                user.Confirmed = true;
                _trialAccounts.FindOneAndReplace(x => x.Id == user.Id, user);
                Console.WriteLine($"TrialConfirmed: SUCCESS");
                return true;
            }
        }
    }
}
