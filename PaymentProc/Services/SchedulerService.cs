﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using PaymentProc.Attributes;
using PaymentProc.Models;
using PaymentProc.ViewModels;
using Stripe;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PaymentProc.Services
{
    public class SchedulerService
    {
        private readonly IConfiguration _configuration;

        private readonly IMongoCollection<TrialAccount> _trialAccounts;
        private readonly IMongoCollection<Models.Environment> _envCollection;
        private readonly IMongoCollection<BillingCustomer> _billingCustomer;
        private readonly IMongoCollection<MessagePrice> _priceCollection;
        private readonly IMongoCollection<PricingPlan> _planCollection;

        private readonly NotificationService _notificationService;

        private readonly string _WATI_DEMO;
        private readonly string _WATI_DEMO_FE;
        private readonly string _googleSheetId;
        private readonly string _googleSheetKeyPath;
        private readonly string _trialToken;
        private readonly string _accountToken;
        private readonly double _supportMessagesPrice;
        private readonly int _defaultTrialPeriod;
        private readonly string _defaultPriceEmailTrialEnd;

        static string[] Scopes = { SheetsService.Scope.Spreadsheets };

        public SchedulerService(IPaymentDatabaseSettings settings, IConfiguration configuration, NotificationService notificationService)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _trialAccounts = database.GetCollection<TrialAccount>("TrialAccount");
            _envCollection = database.GetCollection<Models.Environment>("Environment");
            _billingCustomer = database.GetCollection<BillingCustomer>("BillingCustomer");
            _priceCollection = database.GetCollection<MessagePrice>("MessagePrice");
            _planCollection = database.GetCollection<PricingPlan>("PricingPlan");

            _configuration = configuration;
            _notificationService = notificationService;

            _WATI_DEMO = _configuration["AppSettings:WATI-DEMO"];
            _WATI_DEMO_FE = _configuration["AppSettings:WATI-DEMO-FE"];
            _googleSheetId = _configuration["AppSettings:SPREADSHEET_ID"];
            _googleSheetKeyPath = _configuration["AppSettings:SPREADSHEET_KEY_PATH"];
            _trialToken = _configuration["AppSettings:TrialToken"];
            _accountToken = _configuration["AppSettings:WABusinessAccountToken"];
            _supportMessagesPrice = double.Parse(_configuration["AppSettings:SupportMessagesPrice"]);

            _googleSheetId = _configuration["AppSettings:SPREADSHEET_ID"];
            _googleSheetKeyPath = _configuration["AppSettings:SPREADSHEET_KEY_PATH"];
            _WATI_DEMO = _configuration["AppSettings:WATI-DEMO"];
            _defaultTrialPeriod = int.Parse(_configuration["AppSettings:DefaultTrialPeriod"]);
            _defaultPriceEmailTrialEnd = _configuration["AppSettings:DefaultPriceEmailTrialEnd"];
        }

        public string GeneratePassword()
        {
            int length = 10;

            bool nonAlphanumeric = true;
            bool digit = true;
            bool lowercase = true;
            bool uppercase = true;

            StringBuilder password = new StringBuilder();
            Random random = new Random();

            while (password.Length < length)
            {
                if (nonAlphanumeric)
                    password.Append((char)random.Next(40, 43));
                if (digit)
                    password.Append((char)random.Next(48, 57));
                if (lowercase)
                    password.Append((char)random.Next(97, 122));
                if (uppercase)
                    password.Append((char)random.Next(65, 90));
            }

            return password.ToString();
        }

        private SheetsService GetSheetsService()
        {
            using (var stream = new FileStream(_googleSheetKeyPath, FileMode.Open, FileAccess.Read))
            {
                var serviceInitializer = new BaseClientService.Initializer
                {
                    HttpClientInitializer = GoogleCredential.FromStream(stream).CreateScoped(Scopes)
                };
                return new SheetsService(serviceInitializer);
            }
        }

        [NotRunIfExist]
        public async Task CheckNewRowsGoogleSheets()
        {
            Console.WriteLine("CheckNewRowsGoogleSheets...");

            var service = GetSheetsService();

            int rowStart = 2;

            String spreadsheetId = _googleSheetId;
            String range = $"A{rowStart}:N";
            SpreadsheetsResource.ValuesResource.GetRequest request =
                    service.Spreadsheets.Values.Get(spreadsheetId, range);

            ValueRange response = request.Execute();
            IList<IList<Object>> values = response.Values;
            if (values != null && values.Count > 0)
            {
                Console.WriteLine($"CheckNewRowsGoogleSheets Row Count: {values.Count}");
                
                HttpClient client = new HttpClient();

                string baseUrl = $"{_WATI_DEMO}api/v1/accounts/createTrial?";

                foreach (var row in values)
                {
                    if(row.Count >= 13)
                    {
                        DateTime submitted = DateTime.UtcNow;
                        bool emailConfirmed = false;

                        if (row[11] != null) { 
                            DateTime.TryParse(row[11].ToString(), out submitted);
                        }

                        // only if there is a column with "EmailSent"
                        if (row.Count >= 14 && row[13] != null)
                        {
                            bool.TryParse(row[13].ToString(), out emailConfirmed);
                        }

                        if (emailConfirmed) {
                            rowStart++;
                            continue;
                        }

                        TrialAccount acc = new TrialAccount()
                        {
                            RowInGoogleSheet = rowStart,
                            FirstName = row[0].ToString(),
                            LastName = row[1].ToString(),
                            CompanyName = row[2].ToString(),
                            Email = row[4].ToString(),
                            Phone = Regex.Replace(row[5].ToString(), @"[^\d]+", ""),
                            SubmitedAt = submitted,
                            IsCreated = false
                        };

                        Console.WriteLine($"CheckNewRowsGoogleSheets Email {acc.Email}");

                        var accountExist = _trialAccounts.AsQueryable().Where(x => x.Email == acc.Email).FirstOrDefault();

                        if (accountExist != null)
                        {
                            Console.WriteLine($"Account {acc.Email} already exists! cannot create again");
                            rowStart++;
                            continue;
                        }

                        string password = GeneratePassword();

                        string url = $"{baseUrl}email={acc.Email}&password={password}&firstname={acc.FirstName}&lastname={acc.LastName}&phone={acc.Phone}&token={_trialToken}";

                        Console.WriteLine($"CheckNewRowsGoogleSheets: Create Account with url: {url}");

                        var resp = await client.GetAsync(url);
                        
                        Console.WriteLine($"Response from WATI {resp.StatusCode}");
                        
                        if (resp.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            acc.IsCreated = true;
                            acc.TrialEnd = false;
                            acc.TrialExpiredAt = acc.SubmitedAt.AddDays(_defaultTrialPeriod);
                            acc.Confirmed = false;
                            var valueRange = new ValueRange { Values = new List<IList<object>> { new List<object> { "TRUE" } } };
                            
                            Console.WriteLine($"Update google sheet");
                            
                            // Row M is EmailSent flag
                            var update = service.Spreadsheets.Values.Update(valueRange, spreadsheetId, $"N{rowStart}");
                            update.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.RAW;
                            var res = await update.ExecuteAsync();
                            
                            Console.WriteLine($"Google sheet updated at row {rowStart}");

                            _trialAccounts.InsertOne(acc);
                        }
                    }
                    
                    rowStart++;
                }
            }
        }

        [NotRunIfExist]
        public async Task EnvironmentReady()
        {
            Console.WriteLine("EnvironmentReady...");

            var envs = _envCollection.AsQueryable().Where(x => x.IsActive && !x.SentEmail).ToList();

            foreach (var env in envs)
            {
                var billCustomer = _billingCustomer.AsQueryable().Where(x => x.Id == env.CustomerId).FirstOrDefault();

                if (billCustomer == null)
                    continue;

                Console.WriteLine($"EnvironmentReady: Send BillCustomer {billCustomer.FirstName} ({billCustomer.Email}) Stripe: {billCustomer.StripeCustomerId}");

                if (billCustomer != null)
                {
                    var templateFilePath = @"EmailTemplates/CongratulationTemplate.html";
                    StreamReader objStreamReader = new StreamReader(templateFilePath);
                    var body = objStreamReader.ReadToEnd();
                    objStreamReader.Close();

                    StringBuilder emailTemplate = new StringBuilder(body);

                    emailTemplate.Replace("{firstName}", billCustomer.FirstName);
                    emailTemplate.Replace("{url}", env.Url);
                    emailTemplate.Replace("{year}", DateTime.UtcNow.Year.ToString());

                    var res = await _notificationService.SendMail(billCustomer.Email, "WATI.io - Welcome", emailTemplate.ToString());

                    env.SentEmail = true;
                    _envCollection.FindOneAndReplace(x => x.Id == env.Id, env);
                }
            }
        }

        private async Task<int> GraphAPIRequest(string WABAID, string phoneNumber, long start, long end, string msgType, string countriesList = "")
        {
            string url = $"https://graph.facebook.com/v3.3/{WABAID}?fields=analytics.start({start}).end({end}).granularity(MONTH)" +
                    $".phone_numbers([{phoneNumber}]).country_codes([{countriesList}]).product_types([{msgType}])&access_token={_accountToken}";

            try
            {
                using (HttpClient client = new HttpClient())
                using (var resp = await client.GetAsync(url))
                {
                    if (resp.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        Console.WriteLine($"PaymentService: SchedulerService GraphAPIRequest response StatusCode {resp.StatusCode}");
                        throw new Exception($"PaymentService: SchedulerService API Status code {resp.StatusCode}");
                    }

                    var contents = await resp.Content.ReadAsStringAsync();
                    var analytics = JsonSerializer.Deserialize<WABAResponse>(contents);

                    var result = 0;

                    if (analytics.analytics?.data_points != null)
                    {
                        foreach (var dp in analytics.analytics?.data_points)
                        {
                            result += dp.sent;
                        }
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"PaymentService: SchedulerService GraphAPIRequest exception: {ex.Message}");
                throw;
            }

        }

        private void ChargeAccount(string customerId, string envName, double amount)
        {
            Console.WriteLine($"PaymentService: SchedulerService ChargeAccount for env:{envName} ...");
            var billingCustomer = _billingCustomer.AsQueryable().Where(x => x.Id == customerId).FirstOrDefault();

            if (billingCustomer == null)
            {
                Console.WriteLine($"PaymentService: SchedulerService ChargeAccount for env:{envName} billingCustomer is null");
                throw new Exception($"PaymentService: SchedulerService ChargeAccount for env:{envName} billingCustomer is null");
            }

            try
            {
                var invoiceService = new InvoiceService();

                var invoiceItemOptions = new InvoiceItemCreateOptions
                {
                    Currency = "usd",
                    Amount = (long)(Math.Round(amount, 2) * 100),
                    Customer = billingCustomer.StripeCustomerId,
                    Description = "WhatsApp TeamInbox: template and support messages"
                };

                var invoiceItemService = new InvoiceItemService();
                invoiceItemService.Create(invoiceItemOptions);

                var invoiceCreateOptions = new InvoiceCreateOptions
                {
                    Customer = billingCustomer.StripeCustomerId,
                };

                var invoice = invoiceService.Create(invoiceCreateOptions);

                invoiceService.FinalizeInvoice(invoice.Id);

                invoiceService.Pay(invoice.Id);
            }
            catch (StripeException ex)
            {
                Console.WriteLine($"PaymentService: SchedulerService ChargeAccount exception: {ex.Message}");
                throw;
            }
            Console.WriteLine($"PaymentService: SchedulerService ChargeAccount for env:{envName} ...");
        }

        private List<PriceGroup> GetPriceGroups(string otherCountries)
        {
            var prices = _priceCollection.AsQueryable().GroupBy(x => x.Usd).Select(x => x.Key).ToList();

            var priceGroups = new List<PriceGroup>();
            foreach (var item in prices)
            {
                var tmpCountries = _priceCollection.AsQueryable().Where(x => x.Usd == item && x.Code != otherCountries).Select(x => x.Code).ToList();
                var tmp = new PriceGroup()
                {
                    CountriesList = string.Join(",", tmpCountries),
                    Price = item
                };
                priceGroups.Add(tmp);
            }

            return priceGroups;
        }

        private async Task<double> GetFacebookGraphAnalytics(IEnumerable<PriceGroup> priceGroups, string phoneNumber, string WABAID, long start, long end,
                                                         double coefficient, double otherPriceUsd, double maxPrice, int notificationQuota, int supportQuota)
        {
            const string notificationMsgType = "0";
            const string supportMsgType = "2";

            var knownCountriesMsgCount = 0;
            var notificationChargeAmount = 0.0;

            // notifications (broadcast messages) - get the number of messages only from the priceGroups list
            foreach (var item in priceGroups)
            {
                var sent = await GraphAPIRequest(WABAID, phoneNumber, start, end, notificationMsgType, $"\"{item.CountriesList}\"");
                if(sent != 0)
                {
                    int i = 0;
                }
                knownCountriesMsgCount += sent;
                notificationChargeAmount += sent * item.Price * coefficient;
            }

            // notifications (broadcast messages) - get the total number of messages
            var notificationTotalCount = await GraphAPIRequest(WABAID, phoneNumber, start, end, notificationMsgType);
            if (notificationTotalCount > knownCountriesMsgCount)
            {
                notificationChargeAmount += (notificationTotalCount - knownCountriesMsgCount) * otherPriceUsd * coefficient;
            }

            //customer messages (sessions messages)
            var supportCount = await GraphAPIRequest(WABAID, phoneNumber, start, end, supportMsgType);
            var supportChargeAmount = supportCount * _supportMessagesPrice;

            //calculate
            if (notificationTotalCount > notificationQuota)
            {
                notificationChargeAmount -= notificationQuota * maxPrice;
            }

            if (supportCount > supportQuota)
            {
                supportChargeAmount -= supportQuota * _supportMessagesPrice;
            }

            return notificationChargeAmount + supportChargeAmount;
        }

        private bool IsSubscribed(string billingCustomerId)
        {
            var billCustomer = _billingCustomer.AsQueryable().Where(x => x.Id == billingCustomerId).FirstOrDefault();

            if (billCustomer == null)
            {
                throw new Exception("Billing customer does not exist");
            }

            var plans = _planCollection.AsQueryable().Select(x => x.PlanId).ToList();

            var customerService = new CustomerService();
            var stripeCustomer = customerService.Get(billCustomer.StripeCustomerId);
            var exists = stripeCustomer.Subscriptions.Where(x => plans.Contains(x.Plan.Id)).ToList();

            return exists.Count == 0 ? false : true;
        }

        [NotRunIfExist]
        public async Task ChargeStripe()
        {
            const string otherCountries = "XX";
            const double coefficient = 1.2;
            const long initStartDate = 1546272000;
            Console.WriteLine($"PaymentService: ChargeStripe ...");

            var maxPrice = _priceCollection.AsQueryable().Max(x => x.Usd);
            var otherPrice = _priceCollection.AsQueryable().Where(x => x.Code == otherCountries).FirstOrDefault();
            var now = DateTime.UtcNow;
            var end = new DateTimeOffset(now).ToUnixTimeSeconds();

            var envs = _envCollection.AsQueryable().Where(x => x.IsActive && x.PhoneNumber != null && !string.IsNullOrEmpty(x.WABAID)).ToList();
            var priceGroups = GetPriceGroups(otherCountries);

            foreach (var env in envs)
            {
                Console.WriteLine($"PaymentService: ChargeStripe Start env:{env.EnvironmentName}");
                
                try
                {
                    if (!IsSubscribed(env.CustomerId))
                    {
                        continue;
                    }
                }
                catch(Exception exp)
                {
                    Console.WriteLine($"PaymentService: ChargeStripe exception occurred: env name={env.EnvironmentName}, message={exp.Message}");
                    continue;
                }

                long start = 0;
                if (env.LastBillableDate == null)
                {
                    start = initStartDate;                    
                }
                else
                {
                    var lastDayOfPrevMonth = new DateTime(env.LastBillableDate.Year, env.LastBillableDate.Month, 1).AddDays(-1);
                    start = new DateTimeOffset(lastDayOfPrevMonth).ToUnixTimeSeconds();
                }

                var totalAmount = await GetFacebookGraphAnalytics(priceGroups, env.PhoneNumber, env.WABAID, start, end, coefficient,
                                                                  otherPrice.Usd, maxPrice, env.NotificationQuota, env.SupportQuota);

                if (totalAmount == 0)
                {
                    continue;
                }

                ChargeAccount(env.CustomerId, env.EnvironmentName, totalAmount);

                env.LastBillableDate = now;
                _envCollection.FindOneAndReplace(x => x.Id == env.Id, env);
                Console.WriteLine($"PaymentService: ChargeStripe for env:{env.EnvironmentName} end");
            }

            Console.WriteLine($"PaymentService: ChargeStripe completed");
        }

        public async Task CheckEndTrialAccounts()
        {
            var accounts = _trialAccounts.AsQueryable().Where(x => !x.TrialEnd && x.TrialExpiredAt <= DateTime.UtcNow && x.Confirmed).ToList();
            
            var templateFilePath = @"EmailTemplates/TrialEndedTemplate.html";
            StreamReader objStreamReader;
            var body = "";

            foreach (var acc in accounts)
            {
                if(string.IsNullOrEmpty(body))
                {
                    objStreamReader = new StreamReader(templateFilePath);
                    body = objStreamReader.ReadToEnd();
                    objStreamReader.Close();
                }

                StringBuilder emailTemplate = new StringBuilder(body);

                emailTemplate.Replace("{email}", acc.Email);
                emailTemplate.Replace("{name}", $"{acc.FirstName} {acc.LastName}");
                emailTemplate.Replace("{price}", _defaultPriceEmailTrialEnd);
                emailTemplate.Replace("{url}", $"{_WATI_DEMO_FE}/payment");
                emailTemplate.Replace("{year}", DateTime.UtcNow.Year.ToString());

                var res = await _notificationService.SendMail(acc.Email, "WATI.io - Trial ended", emailTemplate.ToString());
                
                acc.TrialEnd = true;
                _trialAccounts.FindOneAndReplace(x => x.Id == acc.Id, acc);
            }
        }
    }

    public class PriceGroup
    {
        public string CountriesList { get; set; }

        public double Price { get; set; }
    }
}
