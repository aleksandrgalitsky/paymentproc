﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using AirtableApiClient;

namespace PaymentProc.Services
{
    public class NotificationService
    {
        private readonly IConfiguration _configuration;

        private readonly string _sendGridKey;
        private readonly string _airTableBaseId;
        private readonly string _airTableAppKey;
        private readonly string _paymentNotificationEmail;

        public NotificationService(IConfiguration configuration)
        {
            _configuration = configuration;
            _sendGridKey = _configuration["AppSettings:SendGridKey"];
            _airTableBaseId = _configuration["AppSettings:AirTableBaseId"];
            _airTableAppKey = _configuration["AppSettings:AirTableAppKey"];
            _paymentNotificationEmail = _configuration["AppSettings:PaymentNotificationEmail"];
        }

        public async Task<Response> SendMail(string email, string subject, string message)
        {
            var client = new SendGridClient(_sendGridKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("hello@wati.io", "WATI.io"),
                Subject = subject,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));
            string[] emails = _paymentNotificationEmail.Split(',');
            foreach (var supportEmail in emails)
            {
                msg.AddBcc(new EmailAddress(supportEmail));
            }
            var resp = await client.SendEmailAsync(msg);
            return resp;
        }

        public async Task<Response> SendMail(string email, string subject, string message, byte[] File, string fileName)
        {
            var client = new SendGridClient(_sendGridKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("hello@wati.io", "WATI.io Support"),
                Subject = subject,
                PlainTextContent = message,
                HtmlContent = message,
            };
            var file = Convert.ToBase64String(File);
            msg.AddAttachment(fileName, file);

            msg.AddTo(new EmailAddress(email));

            return await client.SendEmailAsync(msg);
        }

        public async Task<string> SendToAirTable(string firstName, string lastName, string companyName, string email, string phone, string stripeId, string notes)
        {
            var records = new List<AirtableRecord>();

            using (AirtableBase airtableBase = new AirtableBase(_airTableAppKey, _airTableBaseId))
            {
                Fields f = new Fields();
                f.AddField("First Name", firstName);
                f.AddField("Last Name", lastName);
                f.AddField("Company Name", companyName == null ? "" : companyName);
                f.AddField("Email", email);
                f.AddField("Phone Number", phone);
                f.AddField("Stripe ID", stripeId);
                f.AddField("Notes", notes);

                var response = await airtableBase.CreateRecord("Table 1", f, true);

                if (!response.Success)
                {
                    string errorMessage = "";
                    if (response.AirtableApiError is AirtableApiException)
                    {
                        errorMessage = response.AirtableApiError.ErrorMessage;
                    }
                    else
                    {
                        errorMessage = "Unknown error";
                    }
                    return errorMessage;
                }
                else
                {
                    var record = response.Record;
                    return "Ok";
                }
            }
        }
    }
}
