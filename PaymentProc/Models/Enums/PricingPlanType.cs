﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models.Enums
{
    public enum PricingPlanType
    {
        Recurring = 0,
        Metered = 1
    }
}
