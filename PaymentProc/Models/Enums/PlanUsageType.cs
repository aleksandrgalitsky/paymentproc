﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models.Enums
{
    public static class PlanUsageType
    {
        public static string Recurring = "licensed";
        public static string Metered = "metered";
    }
}
