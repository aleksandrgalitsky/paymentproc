﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models.Enums
{
    public enum TypeMessages
    {
        Template = 0,
        Support = 1,
        SMS = 2
    }
}
