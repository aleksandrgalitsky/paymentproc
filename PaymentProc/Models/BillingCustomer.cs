﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models
{
    public class BillingCustomer
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Company { get; set; }

        public string StripeCustomerId { get; set; }

        public string PlanId { get; set; }

        public long OperatorsCount { get; set; }
    }
}
