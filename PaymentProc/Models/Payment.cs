﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models
{
    public class Payment
    {
        public string Id { get; set; }

        public string PaymentIntentId { get; set; }

        public string Status { get; set; }

        public string EnviromentName { get; set; }
    }
}
