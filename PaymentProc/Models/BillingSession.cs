﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models
{
    public class BillingSession
    {
        public string Id { get; set; }

        public string SessionId { get; set; }

        public DateTime Created { get; set; }

        public bool IsComplete { get; set; }

        public string SessionGuid { get; set; }

        public string Domain { get; set; }

        public string PaymentIntentId { get; set; }

        public string CustomerId { get; set; }
    }
}
