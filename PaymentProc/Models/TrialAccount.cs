﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models
{
    public class TrialAccount
    {
        public string Id { get; set; }

        public int RowInGoogleSheet { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CompanyName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime SubmitedAt { get; set; }

        public bool IsCreated { get; set; }

        public DateTime TrialExpiredAt { get; set; }

        public bool TrialEnd { get; set; }

        public bool Confirmed { get; set; }
    }
}
