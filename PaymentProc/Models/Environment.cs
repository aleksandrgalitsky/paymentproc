﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models
{
    public class Environment
    {
        public string Id { get; set; }

        public string CustomerId { get; set; }

        public string EnvironmentName { get; set; }

        public bool IsActive { get; set; }

        public bool SentEmail { get; set; }

        public string Url { get; set; }

        public string PlanTier { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime LastBillableDate { get; set; }

        public int NotificationQuota { get; set; }

        public int SupportQuota { get; set; }

        public string CountryCode { get; set; }

        public string WABAID { get; set; }
    }
}
