﻿using PaymentProc.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models
{
    public class PricingPlan
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Period { get; set; }

        public string BillingScheme { get; set; }

        public int OperatorsCount { get; set; }

        public string PlanId { get; set; }
    }
}
