﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.Models
{
    public class MessagePrice
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public double Usd { get; set; }

        public double Eur { get; set; }

        public double Inr { get; set; }

        public double Idr { get; set; }

        public double Gbp { get; set; }

        public double Aud { get; set; }
    }
}
