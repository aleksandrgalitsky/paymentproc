﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.ViewModels
{
    public class WABAResponse
    {
        public Analytics analytics { get; set; }
        public string id { get; set; }
    }

    public class Analytics
    {
        public string[] phone_numbers { get; set; }
        public string granularity { get; set; }
        public Data_Points[] data_points { get; set; }
    }

    public class Data_Points
    {
        public int start { get; set; }
        public int end { get; set; }
        public int sent { get; set; }
        public int delivered { get; set; }
    }

}
