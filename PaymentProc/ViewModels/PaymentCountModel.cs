﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentProc.ViewModels
{
    public class PaymentCountModel
    {
        public string EnvironmentName { get; set; }

        public int Sms { get; set; }

        public int TemplateMessage { get; set; }
        
        public int SupportMessage { get; set; }
    }
}
